<?php

class SteamInventory {

    const API_URL = 'http://steamcommunity.com/id/%s/inventory/json/753/1';
    const IMAGES_URL = 'http://cdn.steamcommunity.com/economy/image/';

    private static $inventoryArray = array();

    public static function getInventory($communityId) {
        $url = sprintf(self::API_URL, $communityId);
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        $games = $data['rgDescriptions'];

        foreach ($games as $game) {
            $stGame = new stdClass();

            if ($game['tradable']) {
                $stGame->name = $game['name'];
                if (count($game['descriptions']) > 1) {
                    $stGame->desc1 = $game['descriptions'][0]['value'];
                    $stGame->desc2 = $game['descriptions'][1]['value'];
                } else {
                    $stGame->desc1 = NULL;
                    $stGame->desc2 = $game['descriptions'][0]['value'];
                }
                $stGame->iconSmall = self::IMAGES_URL . $game['icon_url'];
                $stGame->iconLarge = self::IMAGES_URL . $game['icon_url_large'];
                $stGame->link = $game['actions']['link'];
                self::$inventoryArray[] = $stGame;
            }
        }

        return json_encode(self::$inventoryArray);
    }

}

?>
